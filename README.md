Source of my website
====================

Made with [`blogdown`](https://github.com/rstudio/blogdown) and [Hugo](https://gohugo.io/) thanks to the insightful [`blogdown` book](https://bookdown.org/yihui/blogdown/).

I chose the theme [blackburn](https://github.com/yoshiharuyamashita/blackburn) after seeing it used for [simplystatistics.org blog](https://github.com/rbind/simplystats) and [masalmon.eu blog](https://github.com/maelle/simplymaelle).

