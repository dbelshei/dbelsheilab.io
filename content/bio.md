+++
title = "Bio"
date = "2018-06-11"
sidemenu = "true"
description = ""
+++


<!---
## <a name="pro"></a>Professional experience
* May 2018 - present:  __bold type__ [for link]()
-->

## <a name="professional experience"></a>Professional Experience

* 2024-Present, __Analytics Software Tester__, JMP Statistical Discovery, Cary, NC, USA.


* 2023-2024, __Graduate Intern__, JMP Statistical Discovery, Cary, NC, USA.

* 2021-2023, __Graduate Research Assistant__, JMP Statistical Discovery, Cary, NC, USA.

## <a name="education"></a>Education

* 2018-2024, __PhD student in Statistics__, North Carolina State University, Raleigh, NC, USA.

* 2018-2020, __Master of Statistics__, North Carolina State University, Raleigh, NC, USA.

* 2016-2018,  __BSc in Statistics__, North Carolina State University, Raleigh, NC, USA. Graduated Summa Cum Laude in May 2018.

* 2016-2018,  __BSc in Applied Mathematics__, North Carolina State University, Raleigh, NC, USA. Graduated Valedictorian in May 2018.

## <a name="outreach"></a>Outreach

* 2019-2022 __Vice President (external)__, North Carolina State University Statistics Graduate Student Association

## <a name="languages"></a>Languages

* I'm fluent in [JSL](https://www.jmp.com/support/help/en/18.0/index.shtml#page/jmp/introduction-to-writing-jsl-scripts.shtml). Additionally, I am fluent in [R](https://www.r-project.org/), including package development, more info on my [Github](https://github.com/dbelshei) and [Gitlab](https://gitlab.com/dbelshei) profiles. This website was built with the R package [`blogdown`](https://github.com/rstudio/blogdown).  using [this book](https://bookdown.org/yihui/blogdown/), the [blackburn Hugo theme](https://github.com/yoshiharuyamashita/blackburn), and by looking at [the simplystatistics blog](https://simplystatistics.org) and [Maëlle Salmon's blog](https://masalmon.eu).

* I'm also proficient in LaTeX.

* I'm fluent in SAS.

* My first language is English. I also know a fragment of French, a smattering of Spanish, and a touch of Tagalog. I am also starting to learn Latin.



## Personal Interests

In my free time, I love to read, and am also hoping to work on writing a screenplay in the future. I'm an avid film fan, and have taken up film photography as a hobby.
