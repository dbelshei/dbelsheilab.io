---
author: "David Elsheimer"
date: 2019-08-19
linktitle: Hello again!
title: Hello again!
weight: 10
---

## I passed the qualifying exam!

It's been nearly a year since I've posted here. My first year went not as smoothly as it started out when I last posted, but oerall has gone really well. I've learned a lot and studying for the qualifying exam helped cement a lot of the subject areas I learned this past year. Thankfully, I passed the qual this past week, and will be starting the next year of classes in a few days. I'm really looking forward to this semester, as all of my courses are focusing on topics I'm greatly interested in learning more about. 

If I find time, I will hopefully be posting more about the things I'm learning and any interesting applications of it that I can find time to do outside of class. I'm also starting a year-long term as VP of my departments graduate student assocation this fall, which I am very excited about.

Since my last post, I've been able to implement Disqus commenting, as well as add some more relevant links on the website.  More will be added in the coming months. I hope to be a lot more active posting here in the following months. 