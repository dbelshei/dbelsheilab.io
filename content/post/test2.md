---
author: "David Elsheimer"
date: 2018-09-26
linktitle: September
title: September
weight: 10
---

## The first month of classes is nearly done!

I've been working on fall classes the in the NCSU Ph.D. program for a little over a month now. Things are moving along fairly smoothly, at least in comparison to undergrad.

However, I've found ways to keep busy, and I'm tasked with doing the tech support for our departments online classes, as far as general online course structure.

Unfortunately, this means I've been too busy to do any research outside my studies or tidy up this site. Currently I'll working on implementing Disqus commenting for the site, but efforts to do so have not proved fruitful yet. (If anyone has a fix for that, shoot me an email at davelsh@protonmail.com).

I'm planning on continuing to write a sort of online journal of my graduate journey, and hoping to update it at least monthly, even if the first year of courses is a little bit "boring" (from a research standpoint, realistically it's anything but)
