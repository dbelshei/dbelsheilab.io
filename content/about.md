+++
title = "About"
date = "2018-06-11"
sidemenu = "true"
description = "Some info about me"
+++


<p align="center">
  <img src="../img/website_about.jpg" alt="me" width="300">
</p>

Welcome to my website! Here I'll be posting about topics I find interesting in statistics, as well as other topics that interest me, such as film photography. This website initially served as a blog for my Ph.D. journey, but as that journey was fairly busy, I had almost no time to actually document that journey.

I [currently](/bio/) am a statistical software tester and successfully defended by dissertation August 16th, 2024. Alongside my statistics and film photography interests, I am also beginning to learn more about gardening (as best as I can do in an apartment). Additionally, I am trying to find ways in my life to replace proprietary software usage with open source solutions.
<!--
I also occasionally post on a shared blogging website [Just Let Me Write](https://justletmewrite.org) under the pseudonym **root**. JLMW dev'd by [0x19](https://0x19.org/) anon. Site ui unwieldy by design. -->
